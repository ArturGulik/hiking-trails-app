package com.example.hikingtrails

import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.scrollBy
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Star
import androidx.compose.material.icons.rounded.AddCircle
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Composition
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import androidx.core.view.accessibility.AccessibilityViewCommand.ScrollToPositionArguments
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.room.AutoMigration
import androidx.room.ColumnInfo
import androidx.room.CoroutinesRoom
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Upsert
import com.example.hikingtrails.ui.theme.HikingTrailsTheme
import com.example.hikingtrails.ui.theme.LocalBrushes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

@Entity(tableName = "trails")
data class Trail(
    var name: String,
    var description: String,
    var imageId: Int,

    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,

    @ColumnInfo(name="favorite", defaultValue="false")
    var favorite: Boolean = false,
) {
    fun foundString(searchWord: String): Boolean {
        if (searchWord.isEmpty()) return true
        if (name.contains(searchWord, ignoreCase = true)) return true
        if (description.contains(searchWord, ignoreCase = true)) return true
        return false
    }
}

fun toggleTrailFavorite(trail: Trail): Trail {
    return Trail(trail.name, trail.description, trail.imageId, trail.id, !trail.favorite)
}
@Dao
interface TrailDao {
    @Upsert
    suspend fun upsertTrail(trail: Trail)

    @Delete
    suspend fun deleteTrail(trail: Trail)

    @Query("DELETE FROM trails")
    fun clear()

    @Query("SELECT * FROM trails")
    fun getTrails() : List<Trail>

    @Query("SELECT COUNT(*) FROM trails")
    fun getTrailCount(): Int
}

@Composable
fun TrailListElementCompose(app: App, trail: Trail) {
    val height = if (app.sideView()) 80.dp else 100.dp
    var showDialog by remember { mutableStateOf(false) }

    val rowModifier = if (trail == app.currentTrail)
        Modifier.background(
            //shape= RoundedCornerShape(20.dp),
            brush = if (app.darkTheme) LocalBrushes.current.darkRowBrush else LocalBrushes.current.lightRowBrush
        )
    else Modifier

    Row(
        modifier = rowModifier
            .padding(vertical = 5.dp)
            .clickable {
                if (app.currentTrail != trail && app.isTimerStarted) {
                    showDialog = true
                } else app.openTrail(trail.id)
            },
        horizontalArrangement = Arrangement.Start
    ) {
        Spacer(Modifier.width(20.dp))
        Image(
            modifier = Modifier
                .size(height)
                .clip(RoundedCornerShape(40)),
            contentScale = ContentScale.Crop,
            painter = painterResource(id = trail.imageId),
            contentDescription = null
        )
        Spacer(Modifier.width(20.dp))
        Column(
            modifier = Modifier
                .height(height)
                .fillMaxSize()
                .weight(1f),
            verticalArrangement = Arrangement.SpaceAround
        ) {
            Text(
                text = trail.name,
                fontSize = if (app.sideView()) 20.sp else 25.sp
            )
            if (trail == app.currentTrail && app.isTimerStarted) {
                Row(
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Icon(
                        painterResource(id = if (app.isTimerLive) R.drawable.timer_icon else R.drawable.timer_pause_icon),
                        contentDescription = null,
                        modifier = Modifier.padding(end = 5.dp)
                    )
                    Text(
                        text = if (app.isTimerLive) "Timer running..." else "Timer paused",
                        fontSize = if (app.sideView()) 16.sp else 20.sp,
                        overflow = TextOverflow.Clip
                    )
                }
            }
            else {
                Text(
                    text = trail.description,
                    fontSize = if (app.sideView()) 16.sp else 20.sp,
                    overflow = TextOverflow.Ellipsis
                )
            }
        }
        Spacer(Modifier.width(20.dp))
    }

    if (showDialog) {
        AlertDialog(
            onDismissRequest = {
                showDialog = false
            },
            icon = { Icon(painterResource(id = R.drawable.timer_icon), contentDescription = null) },
            title = {
                Text(text = "Timer is running")
            },
            text = {
                Text(text = "Opening another trail will stop the timer. Your time will be saved.")
            },
            confirmButton = {
                TextButton(
                    onClick = {
                        app.stopTimer()
                        showDialog = false
                        app.openTrail(trail.id)
                    }
                ) {
                    Text("Confirm")
                }
            },
            dismissButton = {
                TextButton(
                    onClick = {
                        showDialog = false
                    }
                ) {
                    Text("Cancel")
                }
            }
        )

    }
}

@Composable
fun TrailDetailCompose(app: App, trail: Trail) {
    var justOpened by remember { mutableStateOf(true) }
    var scrollState by remember {
        mutableStateOf(ScrollState(0))
    }

    var timerEntryCount by remember {
        mutableIntStateOf(0)
    }

    var selfieCount by remember {
        mutableIntStateOf(0)
    }

    val viewModel: SelfieViewModel = viewModel()

    if (!justOpened) {
        LaunchedEffect(timerEntryCount, selfieCount ) {
            scrollState.animateScrollTo(scrollState.maxValue)
        }
    } else {
        viewModel.rmSelfie()
    }

    DisposableEffect(trail.id) {
        onDispose {
            scrollState = ScrollState(0)
        }
    }

    val whiteOrBlack = if (app.darkTheme) Color.White else Color.Black

    val context = LocalContext.current

    // Camera or Gallery launcher
    val openCameraLauncher = rememberLauncherForActivityResult(ActivityResultContracts.TakePicturePreview()) { bitmap ->
        // Handle the result
        bitmap?.let {
            viewModel.setSelfie(it)
            selfieCount++
        }
    }

    val requestPermissionLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                openCameraLauncher.launch(null)
            } else {
                // Permission denied
            }
        }

    Scaffold(
        floatingActionButton = {
        FloatingActionButton(
            modifier = if (app.sideView()) Modifier.size(50.dp) else Modifier.size(70.dp),
            onClick = {
                if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED
                ) {
                    openCameraLauncher.launch(null)
                } else {
                    requestPermissionLauncher.launch(android.Manifest.permission.CAMERA)
                }
            },
            containerColor = if (app.darkTheme) Color(0xFF3700B3) else Color.Gray,
            contentColor = Color.White
        ) {
            Icon(Icons.Filled.Add, contentDescription = "")
        }
    }
    ) { padding ->
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(padding)
                .verticalScroll(scrollState),

            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                modifier = Modifier.padding(20.dp),
                fontSize = 27.sp,
                textAlign = TextAlign.Center,
                text = trail.name
            )
            Image(
                modifier = Modifier,
                contentScale = ContentScale.FillWidth,
                painter = painterResource(id = trail.imageId),
                contentDescription = null
            )
            Row(
                Modifier
                    .fillMaxWidth()
                    .clickable {
                        app.toggleFavorite()
                    },
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    modifier = Modifier,
                    fontSize = 27.sp,
                    textAlign = TextAlign.Right,
                    text = if (trail.favorite) "Favorite" else "Not in favorites"
                )
                Icon(
                    Icons.Default.Star,
                    tint = if (trail.favorite) Color.Yellow else whiteOrBlack,
                    contentDescription = null,
                    modifier = Modifier
                        .padding(horizontal = 5.dp)
                        .size(35.dp)
                )
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 10.dp, vertical = 5.dp)
                    .height(2.dp)
                    .background(color = whiteOrBlack)

            ) {}

            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(10.dp, top = 0.dp),
                horizontalAlignment = Alignment.Start
            ) {
                Text(
                    modifier = Modifier,
                    text = trail.description,
                    fontSize = 24.sp
                )
            }

            val timerValue by app.timer.collectAsState()
            Spacer(Modifier.height(20.dp))
            Timer(
                timerValue,
                onStart = { app.startTimer() },
                onPause = { app.pauseTimer() },
                onStop = {
                    app.stopTimer {
                        timerEntryCount = app.timerEntries[trail.id]?.size ?: 0
                        justOpened = false
                    }
                },
                app = app
            )
            TimerEntryList(app.timerEntries[trail.id]!!, app.darkTheme, removeTrail = {
                app.removeTimerEntry(it)
            })

            // Selfie
            if (viewModel.selfie != null) {
                Column(
                    modifier = Modifier
                        .width(400.dp),
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Image(
                        bitmap = viewModel.selfie!!.asImageBitmap(),
                        contentDescription = "Selfie",
                        modifier = Modifier
                            .size(400.dp)
                            .padding(10.dp, bottom=25.dp)
                    )
                }
            }

            Spacer(modifier = if (app.sideView()) Modifier.height(60.dp) else Modifier.height(80.dp))
        }
    }
}
class SelfieViewModel : ViewModel() {
    private val _selfie = mutableStateOf<Bitmap?>(null)
    val selfie: Bitmap? get() = _selfie.value

    fun rmSelfie() {
        _selfie.value = null
    }

    fun setSelfie(bitmap: Bitmap) {
        _selfie.value = bitmap
    }
}