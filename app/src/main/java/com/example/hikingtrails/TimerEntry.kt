package com.example.hikingtrails

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.Query
import androidx.room.Upsert
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.Calendar
import java.util.Locale

@Entity(tableName = "timer_entries")
data class TimerEntry(
    var trailId: Int,
    @ColumnInfo(name="time", defaultValue="0")
    val time: Long,
    @ColumnInfo(name="date", defaultValue="not known")
    val date: String,
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0
)

data class TopHike(
    val trailName: String,
    val time: Long,
    val date: String
) {
    override fun toString(): String {
        return "$trailName: $time ($date)"
    }
}

@Dao
interface TimerEntryDao {
    @Upsert
    suspend fun upsertTimerEntry(timerEntry: TimerEntry)

    @Delete
    suspend fun deleteTimerEntry(timerEntry: TimerEntry)

    @Query("DELETE FROM timer_entries")
    fun clear()

    @Query("SELECT COUNT(*) FROM timer_entries WHERE trailId = :trailId")
    fun getTimerEntriesCount(trailId: Int) : Int

    @Query("SELECT * FROM timer_entries WHERE trailId = :trailId")
    fun getTimerEntries(trailId: Int) : List<TimerEntry>

    @Query("SELECT trails.name as trailName, timer.time, timer.date FROM timer_entries timer INNER JOIN trails trails ON timer.trailId = trails.id ORDER BY timer.time DESC LIMIT 3")
    fun getTop3TimerEntries(): List<TopHike>
}

@Composable
fun InfoBox(text: String) {
    Row(
        Modifier.fillMaxSize(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                modifier = Modifier.padding(10.dp),
                text = text,
                textAlign = TextAlign.Center,
                fontSize = 25.sp,
                color = Color.Gray
            )
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun TimerEntryCompose(
    timerEntry: TimerEntry,
    darkMode: Boolean,
    index: Int = 0,
    fontSize: TextUnit = 25.sp,
    onLongClick: () -> Unit = {}
) {
    val oddColor = if (darkMode) Color(0xFF424242) else Color.LightGray
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth()
            .combinedClickable(
                onClick = {},
                onLongClick = onLongClick
            )
            .background(color = if (index % 2 == 1) oddColor else Color.Transparent )
            .padding(10.dp)

    ) {
        Text(timerEntry.date, fontSize = fontSize)
        Text(formatTime(timerEntry.time), fontSize = fontSize)
    }
}

fun currentDateString() : String {
    val date = Calendar.getInstance().time
    return SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault()).format(date)
}

@Composable
fun TimerEntryList(entriesList: List<TimerEntry>, darkMode: Boolean, removeTrail: (TimerEntry) -> Unit = {}) {
    println("Recomposition of the TimerEntryList. Element count: " +
            "${entriesList.size}")
    var selection: TimerEntry? by remember { mutableStateOf(null) }

    Text("Recorded sessions",
        fontSize = 35.sp,
        modifier = Modifier.padding(bottom = 10.dp))

    val fontSize = 25.sp
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)

    ) {
        Text("Date", fontSize = fontSize)
        Text("Session time", fontSize = fontSize)
    }
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(2.dp)
            .padding(horizontal = 10.dp)
            .background(color = if (darkMode) Color.White else Color.Black)

    ){}

    if (entriesList.isEmpty()) {
        InfoBox("No timer entries")
        Spacer(Modifier.size(10.dp))
    }
    else {
        Column(
            modifier = Modifier.padding(bottom = 20.dp)
        ) {
            entriesList.forEachIndexed { index, timerEntry ->
                TimerEntryCompose(timerEntry, darkMode, index) {
                    selection = timerEntry
                }
            }
        }
    }

    if (selection != null) {
        AlertDialog(
            onDismissRequest = {
                selection = null
            },
            icon = { Icon(painterResource(id = R.drawable.timer_icon), contentDescription = null) },
            title = {
                Text(text = "Entry deletion")
            },
            text = {
                Column() {
                    Text(text = "Would you like to remove this measurement?",
                        fontSize = 20.sp)
                    Spacer(Modifier.size(10.dp))
                    TimerEntryCompose(selection!!, darkMode, fontSize=20.sp)
                    Spacer(Modifier.size(10.dp))
                    Text(text = "This action is irreversible.", fontSize = 20.sp)
                }
            },
            confirmButton = {
                TextButton(
                    onClick = {
                        removeTrail(selection!!)
                        selection = null
                    }
                ) {
                    Text("Confirm")
                }
            },
            dismissButton = {
                TextButton(
                    onClick = {
                        selection = null
                    }
                ) {
                    Text("Cancel")
                }
            }
        )
    }
}