package com.example.hikingtrails

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

fun formatTime(time: Long): String {
    val hours = time / 3600
    val minutes = (time % 3600) / 60
    val remainingSeconds = time % 60
    return String.format("%02d:%02d:%02d", hours, minutes, remainingSeconds)
}

fun formatTimeHours(time: Long): String {
    val hours = time / 3600
    val minutes = (time % 3600) / 60
    return String.format("%02d:%02dm", minutes)
}

fun formatTimeMinutes(time: Long): String {
    val minutes = (time % 3600) / 60
    val remainingSeconds = time % 60
    return String.format("%02d:%02ds", minutes, remainingSeconds)
}

fun formatTimeSeconds(time: Long): String {
    val seconds = time % 60
    return "${seconds}s"
}

fun dateToDayAndMonth(date: String): String {
    val splitted = date.split("/")
    return splitted[0] + "/" + splitted[1]
}

@Composable
fun EasyIconButton(
    buttonText: String,
    onClickCallback: () -> Unit,
    iconResourceId: Int,
    // The width of the row the text is put in for centering.
    // If null,
    centerTextWidth: Dp? = null
) {
    Button(onClick = onClickCallback) {
        Icon(
            painterResource(id = iconResourceId),
            modifier = Modifier
                .padding(end = 10.dp)
                .size(30.dp),
            contentDescription = null
        )
        Row(
            modifier = if (centerTextWidth == null) Modifier else Modifier.width(centerTextWidth),
            horizontalArrangement = Arrangement.Center
        ){
            Text(buttonText)
        }
    }
}

@Composable
fun Timer(
    timerValue: Long,
    onStart: () -> Unit,
    onPause: () -> Unit,
    onStop: () -> Unit,
    app: App
    ) {
    val iconModifier = Modifier
        .padding(end = 10.dp)
        .size(30.dp)

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painterResource(id = R.drawable.timer_icon),
                modifier = Modifier.padding(end=10.dp),
                contentDescription = null
            )
            Text(text = formatTime(timerValue), fontSize = 24.sp)
        }
        Spacer(modifier = Modifier.height(16.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            if (!app.isTimerStarted) {
                EasyIconButton("Start", onStart, R.drawable.not_started_icon)
            }
            if (app.isTimerStarted) {
                if (app.isTimerLive) {
                    EasyIconButton("Pause", onPause, R.drawable.pause_circle_icon, 60.dp)
                }
                else {
                    EasyIconButton("Resume", onStart, R.drawable.play_circle_icon, 60.dp)
                }
                Spacer(modifier = Modifier.width(16.dp))

                EasyIconButton("Stop", onStop, R.drawable.stop_circle_icon)
            }
        }
        Spacer(modifier = Modifier.size(10.dp))
    }
}
