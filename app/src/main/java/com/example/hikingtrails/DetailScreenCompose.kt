package com.example.hikingtrails


import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun DetailScreenCompose(app: App, onlyShowFavorite: Boolean = false) {
    if (app.currentTrail == null || (onlyShowFavorite && !app.currentTrail!!.favorite)) {
        Row(
            Modifier.fillMaxSize().padding(60.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Row(
                Modifier.border(width = 2.dp, color = Color(0xFFB3B3B3), shape = RoundedCornerShape(15.dp)),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    modifier = Modifier.padding(20.dp),
                    text = "Select a trail to display",
                    textAlign = TextAlign.Center,
                    fontSize = 25.sp,
                    color = Color.Gray
                )
            }
        }
    }
    else {
        TrailDetailCompose(app, app.currentTrail!!)
    }
}