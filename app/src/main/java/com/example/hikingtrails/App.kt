package com.example.hikingtrails

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.runtime.snapshots.SnapshotStateMap
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavHostController
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

enum class AppState {
    FAVORITES, HOME, ALL
}

class App: ViewModel() {
    var windowInfo: WindowInfo? = null
    private var trailDao: TrailDao? = null
    private var timerEntryDao: TimerEntryDao? = null
    private var navController: NavHostController? = null
    private val _timer = MutableStateFlow(0L)

    var trails: List<Trail> = listOf()

    var darkTheme by mutableStateOf(false)
        private set

    var timerEntries by mutableStateOf(mapOf<Int, List<TimerEntry>>())
        private set

    var topHikes by mutableStateOf(listOf<TopHike>())
        private set

    var currentTrail: Trail? by mutableStateOf(null)
        private set

    var isTimerStarted: Boolean by mutableStateOf(false)
        private set

    // Is the timer started and not currently paused?
    var isTimerLive: Boolean by mutableStateOf(false)
        private set

    var appState: AppState by mutableStateOf(AppState.HOME)
        private set

    var searchWord by mutableStateOf("")

    val timer = _timer.asStateFlow()
    private var timerJob: Job? = null

    fun startTimer() {
        timerJob?.cancel()
        timerJob = viewModelScope.launch {
            while(true) {
                delay(1000)
                _timer.value++
            }
        }
        isTimerStarted = true
        isTimerLive = true
    }

    fun pauseTimer() {
        timerJob?.cancel()
        isTimerLive = false
    }

    fun stopTimer(syncCallback: () -> Unit = {}) {
        val timeMeasured = _timer.value

        _timer.value = 0
        timerJob?.cancel()
        isTimerStarted = false
        isTimerLive = false

        val trailId = currentTrail?.id ?: return
        viewModelScope.launch(Dispatchers.IO) {
            timerEntryDao!!.upsertTimerEntry(
                TimerEntry(trailId, timeMeasured, currentDateString())
            )
            println("UPSERTED! CURRENT COUNT:" +
                    " ${timerEntryDao!!.getTimerEntriesCount(trailId)}")
            syncTimerEntries(trailId)

            syncCallback()
        }
    }

    override fun onCleared() {
        super.onCleared()
        timerJob?.cancel()
    }

    fun setNavController(controller: NavHostController) {
        if (navController == controller) {
            // navController didn't change, nothing to do
            return
        }
        navController = controller


        if (sideView() && navController!!.currentDestination?.route == Screen.DetailScreen.route) {
            navController!!.popBackStack()
        }

        if (!sideView() && currentTrail != null && appState != AppState.HOME)
            navController!!.navigate(Screen.DetailScreen.route)
    }

    fun openTrail(trailId: Int) {
        currentTrail = trails.find { it.id == trailId }
        if (!sideView())
            navController?.navigate(Screen.DetailScreen.route)
    }

    fun sideView() : Boolean {
        if (windowInfo == null) return false
        return windowInfo!!.screenWidthType != WindowInfo.WindowType.Compact
    }

    // The activity has been reset. Clear all outside references
    fun reset(newTrailDao: TrailDao, newTimerEntryDao: TimerEntryDao) {
        windowInfo = null
        currentTrail =  null
        navController = null
        trailDao = newTrailDao
        timerEntryDao = newTimerEntryDao

        viewModelScope.launch(Dispatchers.IO) {
            if (trailDao!!.getTrailCount() == 0) {
                fillTrailDao(trailDao!!)
            }

            trails = trailDao!!.getTrails()

            trails.forEach { trail ->
                syncTimerEntries(trail.id)
            }
        }
    }

    private fun syncTimerEntries(trailId: Int) {
        val newTimerEntries = timerEntries.toMutableMap()
        newTimerEntries[trailId] = timerEntryDao!!.getTimerEntries(trailId)

        timerEntries = newTimerEntries.toMap()

        topHikes = timerEntryDao!!.getTop3TimerEntries()
    }

    fun removeTimerEntry(timerEntry: TimerEntry) {
        viewModelScope.launch(Dispatchers.IO) {
            timerEntryDao!!.deleteTimerEntry(timerEntry)
            syncTimerEntries(timerEntry.trailId)
        }
    }

    fun goToFavorites() {
        appState = AppState.FAVORITES
    }

    fun goToWholeList() {
        appState = AppState.ALL
    }

    fun goHome() {
        appState = AppState.HOME
    }

    fun toggleFavorite() {
        currentTrail = toggleTrailFavorite(currentTrail!!)

        viewModelScope.launch(Dispatchers.IO) {
            trailDao!!.upsertTrail(currentTrail!!)

            trails = trailDao!!.getTrails()

            if (currentTrail != null)
                currentTrail = trails.find { it.id == currentTrail!!.id }
        }
    }

    fun toggleDarkMode() {
        darkTheme = !darkTheme
    }

}