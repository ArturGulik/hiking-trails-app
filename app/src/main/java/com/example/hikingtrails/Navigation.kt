package com.example.hikingtrails

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.pager.HorizontalPager
import androidx.compose.foundation.pager.PageSize
import androidx.compose.foundation.pager.rememberPagerState
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import kotlinx.coroutines.launch

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun Navigation(app: App) {
    val scope = rememberCoroutineScope()
    val navController = rememberNavController()
    val pageState = rememberPagerState(initialPage = 1) {
        return@rememberPagerState 3
    }

    NavHost(navController = navController,
        startDestination = Screen.HomeScreen.route) {
        composable(route = Screen.HomeScreen.route) {
            Scaffold(
                bottomBar = {
                    MyBottomAppBar(
                        current = pageState.currentPage,
                        app,
                        onFavorite = {
                            scope.launch {
                                pageState.animateScrollToPage(0)
                            }
                        },
                        onHome = {
                            scope.launch {
                                pageState.animateScrollToPage(1)
                            }
                        },
                    ) {
                        scope.launch {
                            pageState.animateScrollToPage(2)
                        }
                    }
                },
            ) { innerPadding ->
                HorizontalPager(state = pageState, pageSize = PageSize.Fill) { page ->
                    MainAppScreen(app, page, innerPadding)
                }
            }
        }
        composable(
            route = Screen.DetailScreen.route) {
            DetailScreenCompose(app, false)
        }
    }

    app.setNavController(navController)
}