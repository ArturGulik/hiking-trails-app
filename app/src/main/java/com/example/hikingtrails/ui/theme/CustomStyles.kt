package com.example.hikingtrails.ui.theme

import androidx.compose.runtime.compositionLocalOf
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color

data class Brushes(
    val lightRowBrush: Brush = Brush.horizontalGradient(listOf(Color(0xFFABABFF), Color.White)),
    val darkRowBrush: Brush = Brush.horizontalGradient(listOf(Color(0xFF545454), Color(0x00000000)))
)

val LocalBrushes = compositionLocalOf { Brushes() }