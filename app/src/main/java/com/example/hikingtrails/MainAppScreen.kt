package com.example.hikingtrails

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.paint
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.hikingtrails.ui.theme.LocalBrushes

@Composable
fun AllTrailsList(app: App, innerPadding: PaddingValues) {
    Row(
        modifier = Modifier.padding(innerPadding),
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {
            Text(
                modifier = Modifier.padding(15.dp),
                text = "Hiking Trails",
                textAlign = TextAlign.Center,
                fontSize = if (app.sideView()) 25.sp else 32.sp,
            )
            Row(
                modifier = Modifier.padding(10.dp).padding(horizontal=15.dp).padding(bottom=15.dp),
                verticalAlignment = Alignment.CenterVertically
            ){
                Icon(
                    Icons.Filled.Search,
                    contentDescription = "",
                    modifier = Modifier.size(30.dp)
                )
                // Text input
                TextField(
                    value = app.searchWord,
                    onValueChange = { app.searchWord = it },
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(50.dp)
                        .padding(horizontal = 10.dp),
                    maxLines = 1,
                )
            }
            LazyVerticalGrid(
                verticalArrangement = Arrangement.spacedBy(5.dp),
                columns = GridCells.Fixed(1),
                modifier = Modifier
                    .fillMaxSize()

            ) {
                app.trails.filter { it.foundString(app.searchWord) }.forEach { trail ->
                    item {
                        TrailListElementCompose(app, trail)
                    }
                }
                item {
                    Spacer(Modifier.size(0.dp))
                }
            }
        }
        if (app.sideView()) {
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            ) {
                DetailScreenCompose(app = app, onlyShowFavorite = false)
            }
        }
    }
}

@Composable
fun FavoriteTrailsList(app: App, innerPadding: PaddingValues) {
    Row(
        modifier = Modifier.padding(innerPadding),
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .weight(1f),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {
            Text(
                modifier = Modifier.padding(15.dp),
                text = "Favorite Trails",
                textAlign = TextAlign.Center,
                fontSize = if (app.sideView()) 25.sp else 32.sp,
            )
            LazyVerticalGrid(
                verticalArrangement = Arrangement.spacedBy(5.dp),
                columns = GridCells.Fixed(1),
                modifier = Modifier
                    .fillMaxSize()

            ) {
                app.trails.filter { it.favorite }.forEach { trail ->
                    item {
                        TrailListElementCompose(app, trail)
                    }
                }
                if (app.trails.filter { it.favorite }.isEmpty()) {
                    item {
                        Row(
                            Modifier
                                .fillMaxSize()
                                .padding(60.dp),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Row(
                                Modifier.border(width = 2.dp, color = Color(0xFFB3B3B3), shape = RoundedCornerShape(15.dp)),
                                verticalAlignment = Alignment.CenterVertically,
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    modifier = Modifier.padding(20.dp),
                                    text = "Your favorite trails will appear here",
                                    textAlign = TextAlign.Center,
                                    fontSize = 25.sp,
                                    color = Color.Gray
                                )
                            }
                        }
                    }
                }
                item {
                    Spacer(Modifier.size(0.dp))
                }
            }
        }
        if (app.sideView()) {
            Row(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            ) {
                DetailScreenCompose(app = app, onlyShowFavorite=true)
            }
        }
    }
}

@Composable
fun MainAppScreen(app: App, viewId: Int, innerPadding: PaddingValues) {

        when(viewId) {
            0 -> {
                FavoriteTrailsList(app = app, innerPadding = innerPadding)
            }
            1 -> {
                HomeScreen(app)
            }
            2 -> {
                AllTrailsList(app = app, innerPadding = innerPadding)
            }
        }

}

@Composable
fun HomeScreen(app: App) {
    Column(
        Modifier.fillMaxSize()
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Spacer(Modifier.width(60.dp))
            Box(modifier = Modifier.weight(1f)) {
                Text(
                    modifier = Modifier
                        .align(Alignment.Center)
                        .padding(15.dp)
                        .fillMaxWidth(),
                    text = "Hiking Trails",
                    textAlign = TextAlign.Center,
                    fontSize = if (app.sideView()) 37.sp else 32.sp,
                )
            }
            Box(
                modifier = Modifier
                    .padding(end = 20.dp)
                    .size(40.dp)
            ) {
                IconButton(
                    modifier = Modifier,
                    //.background(if (current == 0) Color.LightGray else Color.Transparent),
                    onClick = { app.toggleDarkMode() }
                ) {
                    Icon(
                        painterResource(id = if (app.darkTheme) R.drawable.dark_mode else R.drawable.light_mode),
                        modifier = Modifier.size(60.dp),
                        contentDescription = "Localized description",
                    )
                }
            }
        }

        val rankingBackground = if (app.darkTheme)
            Color(0f, 0f, 0f, 0.8f)
        else Color(1.0f, 1.0f, 1.0f, 0.8f)

        // Top 3
        Column(
            Modifier
                .fillMaxSize()
                .paint(painterResource(id = R.drawable.hiking_background)),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
        ) {
            Column(
                Modifier
                    .clip(shape = RoundedCornerShape(20.dp))
                    .background(rankingBackground)
                    .padding(bottom = 5.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    modifier = Modifier
                        .padding(horizontal = 15.dp),
                    text = if (app.topHikes.isEmpty()) "No Recent Hikes"
                    else if (app.topHikes.size == 1) "Recent Hike:"
                    else "Top ${app.topHikes.size} Recent Hikes",
                    textAlign = TextAlign.Center,
                    fontSize = if (app.sideView()) 34.sp else 32.sp,
                )
                app.topHikes.forEach { topHike ->
                    Text(
                        modifier = Modifier
                            .padding(horizontal = 15.dp),
                        text = dateToDayAndMonth(topHike.date) + " " + topHike.trailName + " - " +
                                if (topHike.time >= 3600) formatTimeHours(topHike.time) else
                                    if (topHike.time >= 60) formatTimeMinutes(topHike.time) else formatTimeSeconds(
                                        topHike.time
                                    ),
                        textAlign = TextAlign.Left,
                        fontSize = if (app.sideView()) 25.sp else 21.sp,
                    )
                }
            }
        }
    }
}

@Composable
fun MyBottomAppBar(
    current: Int,
    app: App,
    onFavorite: () -> Unit,
    onHome: () -> Unit,
    onAll: () -> Unit
) {
    BottomAppBar(
        modifier = Modifier.height(50.dp),
        contentPadding = PaddingValues(0.dp),
        actions = {
            val selectedColor = if (app.darkTheme) Color(0xff3700b3) else Color.LightGray
            Row(
                modifier = Modifier.fillMaxSize(),
                horizontalArrangement = Arrangement.SpaceAround
            ) {
                IconButton(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(1f)
                        .background(if (current == 0) selectedColor else Color.Transparent),
                    onClick = onFavorite
                ) {
                    Icon(
                        Icons.Filled.Favorite,
                        contentDescription = "Localized description",
                    )
                }
                IconButton(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(1f)
                        .background(if (current == 1) selectedColor else Color.Transparent),
                    onClick = onHome
                ) {
                    Icon(Icons.Filled.Home, contentDescription = "Localized description")
                }
                IconButton(
                    modifier = Modifier
                        .fillMaxSize()
                        .weight(1f)
                        .background(if (current == 2) selectedColor else Color.Transparent),
                    onClick = onAll
                ) {
                    Icon(
                        Icons.Filled.Menu,
                        contentDescription = "Localized description",
                    )
                }
            }
        }
    )
}