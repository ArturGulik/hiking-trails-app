package com.example.hikingtrails

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable

abstract class Screen(var route: String) {
    data object HomeScreen : Screen("home")
    data object DetailScreen : Screen("detail_screen")

    @Composable
    open fun Compose() {
        Text (
            text = "This screen's content is not created yet"
        )
    }

    fun routeWithArgs(vararg args: Any): String {
        return buildString {
            append(route)
            args.forEach { arg ->
                append("/$arg")
            }
        }
    }
}