package com.example.hikingtrails

import android.content.Context
import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.AutoMigrationSpec
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(
    entities = [Trail::class, TimerEntry::class],
    version = 5,
    autoMigrations = [
        AutoMigration(2,3),
        AutoMigration(3,4),
        AutoMigration(4, 5)
    ]
)

abstract class RoomDb : RoomDatabase() {

    abstract val trailDao: TrailDao
    abstract val timerEntryDao: TimerEntryDao

    companion object {
        val migration1to2 = object : Migration(1, 2) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL("CREATE TABLE IF NOT EXISTS timer_entries" +
                        " (trailId INTEGER NOT NULL, id INTEGER NOT NULL PRIMARY KEY)")
            }
        }
    }

}

suspend fun fillTrailDao(dao: TrailDao) {
    dao.clear()
    dao.upsertTrail(
        Trail(
            name ="Malta Lake Loop",
            description = "Explore this 6.1-km loop trail near Poznań, Greater Poland. Generally considered an easy route, it takes an average of 1 h 11 min to complete. This is a very popular area for hiking, road biking, and running, so you'll likely encounter other people while exploring. The trail is open year-round and is beautiful to visit anytime. Dogs are welcome, but must be on a leash.",
            imageId = R.drawable.lake_malta
        )
    )
    dao.upsertTrail(
        Trail(
            name ="Warta Left Bank",
            description = "Check out this 3.4-km loop trail near Poznań, Greater Poland. Generally considered an easy route, it takes an average of 48 min to complete. This is a popular trail for running, walking, and bike touring, but you can still enjoy some solitude during quieter times of day. Dogs are welcome, but must be on a leash.",
            imageId = R.drawable.warta_left_bank)
    )
    dao.upsertTrail(
        Trail(
            name ="Puszczykowo - Góreckie Lake",
            description = "Try this 13.7-km loop trail near Puszczykowo, Greater Poland. Generally considered a moderately challenging route, it takes an average of 3 h 9 min to complete. This is a popular trail for hiking, running, and bike touring, but you can still enjoy some solitude during quieter times of day. The trail is open year-round and is beautiful to visit anytime.",
            imageId = R.drawable.lake_goreckie)
    )
    dao.upsertTrail(
        Trail(
            name ="Rogalin Bicycle Trail",
            description = "Experience this 17.7-km loop trail near Rogalinek, Greater Poland. Generally considered a moderately challenging route. This trail is great for bike touring, and it's unlikely you'll encounter many other people while exploring.",
            imageId = R.drawable.rogalin_bicycle_trail)
    )
    dao.upsertTrail(
        Trail(
            name ="Rogalinek Along the Warta River",
            description = "Check out this 18.8-km loop trail near Rogalinek, Greater Poland. Generally considered a moderately challenging route, it takes an average of 4 h 12 min to complete. This is a popular trail for hiking, mountain biking, and running, but you can still enjoy some solitude during quieter times of day. The trail is open year-round and is beautiful to visit anytime. Dogs are welcome, but must be on a leash.",
            imageId = R.drawable.rogalinek_along_river)
    )
    dao.upsertTrail(
        Trail(
            name ="Na Tropach Lądolodu Didactic Path",
            description = "Try this 9.3-km out-and-back trail near Mosina, Greater Poland. Generally considered a moderately challenging route, it takes an average of 2 h 13 min to complete. This trail is great for hiking, and it's unlikely you'll encounter many other people while exploring. You'll need to leave pups at home — dogs aren't allowed on this trail.",
            imageId = R.drawable.na_tropach_ladolodu)
    )
}