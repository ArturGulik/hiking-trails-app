package com.example.hikingtrails

import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.View
import android.view.animation.OvershootInterpolator
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.core.animation.doOnEnd
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.room.Room
import com.example.hikingtrails.ui.theme.HikingTrailsTheme

class MainActivity : ComponentActivity() {
    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            RoomDb::class.java,
            "trails.db"
        ).addMigrations(RoomDb.migration1to2).build()
    }
    private val app by viewModels<App>()

    override fun onCreate(bundle: Bundle?) {
        installSplashScreen().apply {
            setOnExitAnimationListener {screen ->
                val zoomX = ObjectAnimator.ofFloat(
                    screen.iconView,
                    View.SCALE_X,
                    1f,
                    0.0f
                )
                zoomX.interpolator = OvershootInterpolator()
                zoomX.duration = 500L
                zoomX.doOnEnd { screen.remove() }

                val zoomY = ObjectAnimator.ofFloat(
                    screen.iconView,
                    View.SCALE_Y,
                    1f,
                    0.0f
                )
                zoomY.interpolator = OvershootInterpolator()
                zoomY.duration = 500L
                zoomY.doOnEnd { screen.remove() }

                zoomX.start()
                zoomY.start()
            }
        }

        //setTheme(R.style.Theme_App_Starting)
        theme.applyStyle(R.style.Theme_HikingTrails, true)

        super.onCreate(bundle)

        theme.applyStyle(R.style.Theme_HikingTrails, true)

        app.reset(db.trailDao, db.timerEntryDao)

        if (bundle != null && bundle.getInt("trailId") != 0) {
            app.openTrail(bundle.getInt("trailId") - 1)
        }

        compose()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        // Adding 1 to omit special value 0 (meaning value not present)
        // Decreased by 1 when used in openTrail()
        app.currentTrail?.let { outState.putInt("trailId", it.id + 1) }
    }

    fun compose() {
        setContent {
            HikingTrailsTheme(darkTheme = app.darkTheme) {
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    app.windowInfo = rememberWindowInfo()
                    Navigation(app)
                }
            }
        }
    }
}