package com.example.hikingtrails

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun rememberWindowInfo(): WindowInfo {
    val conf = LocalConfiguration.current
    return WindowInfo(
        screenWidthType = when {
            conf.screenWidthDp < 600 -> WindowInfo.WindowType.Compact
            conf.screenWidthDp < 840 -> WindowInfo.WindowType.Medium
            else -> WindowInfo.WindowType.Expanded
        },
        screenHeightType = when {
            conf.screenHeightDp < 480 -> WindowInfo.WindowType.Compact
            conf.screenHeightDp < 900 -> WindowInfo.WindowType.Medium
            else -> WindowInfo.WindowType.Expanded
        },
        screenWidth = conf.screenWidthDp.dp,
        screenHeight = conf.screenHeightDp.dp
    )
}

data class WindowInfo(
    val screenWidthType: WindowType,
    val screenHeightType: WindowType,
    val screenWidth: Dp,
    val screenHeight: Dp
) {
    sealed class WindowType {
        object Compact : WindowType()
        object Medium : WindowType()
        object Expanded: WindowType()
    }
}